from model_api import ModelInterface

if __name__ == "__main__":
    price_str = input("輸入文字：")
    itf = ModelInterface(bert_config_file ='LocalStorage/bert_wwm/bert_config.json',
                         output_dir ='LocalStorage/output/',
                         vocab_file='LocalStorage/bert_wwm/vocab.txt',
                         init_checkpoint ='LocalStorage/bert_wwm/bert_model.ckpt')
    print(itf.predict(price_str.split(",")))