import asyncio

from util import redis_to_MDL, redis_to_API
from model_api import ModelInterface
import json


async def get_work():
    res = await redis_to_MDL.dequeue_with_json_nowait()
    if res is None:
        return None, None
    print(f"get_work: {res}")
    return res['eid'], res['data']


async def send_responds(eid, **item):
    print(f"send_responds: {item}")
    return await redis_to_API.set_msg_by_direct_id_ex(eid, 60, **item)
    # return await redis_to_API.enqueue(item)

ITF = ModelInterface(bert_config_file ='LocalStorage/bert_wwm/bert_config.json',
                     output_dir ='LocalStorage/output/',
                     vocab_file='LocalStorage/bert_wwm/vocab.txt',
                     init_checkpoint ='LocalStorage/bert_wwm/bert_model.ckpt')


def run_model(price_str):
    print(f"1:run_model: {price_str}")
    texts = price_str.split(",")
    res = ITF.predict(texts)
    print(f"2:run_model: {res}")
    return texts,res


async def main():
    while True:
        eid, task = await get_work()
        if task is not None:
            texts,model_res = run_model(task)
            res_dict = {}
            while len(texts) != 0:
                text = texts.pop()
                text_res = model_res.pop()
                res_dict[text] = text_res
            await send_responds(eid, **res_dict)
        await asyncio.sleep(1)


asyncio.run(main())