# coding=utf-8
# Copyright 2018 The Google AI Language Team Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""BERT finetuning runner."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import os
from .utils import modeling, tokenization
import tensorflow as tf
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import metrics_impl
from tensorflow.python.ops import variable_scope
from tensorflow.python.distribute import distribution_strategy_context



task_name = "customized"

do_lower_case = True
max_seq_length =64
use_tpu = False
use_one_hot_embeddings = False
do_train = False
do_eval = False
do_predict = True
train_batch_size = 16
eval_batch_size = 16
predict_batch_size = 16
learning_rate = 5e-5
num_train_epochs = 5
warmup_proportion = 0.1
save_checkpoints_steps = 1000
iterations_per_loop = 1000
master = None
num_tpu_cores = 8
crf_loss_method=True
do_export = False
export_dir = None
alpha = 0.5  # adjust weight between slot and intent, loss = alpha*(slot loss) + (1-alpha)*(intent loss)
dataset = ["train.tsv", "train.tsv", "test.tsv"]


####
class InputExample(object):
    """A single training/test example for simple sequence classification."""

    def __init__(self, guid, text, label=None):
        """Constructs a InputExample.

        Args:
          guid: Unique id for the example.
          text_a: string. The untokenized text of the first sequence. For single
            sequence tasks, only this sequence must be specified.
          text_b: (Optional) string. The untokenized text of the second sequence.
            Only must be specified for sequence pair tasks.
          label: (Optional) string. The label of the example. This should be
            specified for train and dev examples, but not for test examples.
        """
        self.guid = guid
        self.text = text
        self.label = label


class PaddingInputExample(object):
    """Fake example so the num input examples is a multiple of the batch size.

    When running eval/predict on the TPU, we need to pad the number of examples
    to be a multiple of the batch size, because the TPU requires a fixed batch
    size. The alternative is to drop the last batch, which is bad because it means
    the entire output data won't be generated.

    We use this class instead of `None` because treating `None` as padding
    battches could cause silent errors.
    """


class InputFeatures(object):
    """A single set of features of data."""

    def __init__(self,
                 input_ids,
                 input_mask,
                 segment_ids,
                 label_ids,
                 is_real_example=True):
        self.input_ids = input_ids
        self.input_mask = input_mask
        self.segment_ids = segment_ids
        self.label_ids = label_ids
        self.is_real_example = is_real_example



class customizedProcessor():

    def get_strs_examples(self, strs,output_dir):
        label_map, num_labels = self.get_labels_info(output_dir)
        encode_strings = [[f'[{list(label_map.keys())[0]}]',s]for s in strs]

        """See base class."""
        return self._create_examples(encode_strings, "test")

    def get_labels_info(self,output_dir):
        label_map_file = os.path.join(output_dir, "label_map.txt")
        label_map = {}
        labels = []
        with open(label_map_file) as f:
            for row in f.readlines():
                cu = row.replace("\n","").split(":")
                idx = cu[0]
                label_text = cu[1]

                #
                labels.append(label_text)
                label_map[label_text] = int(idx)

        label_map = {'事物說明': 0, '他人描述': 1, '自我探討': 2}
        labels = ['事物說明', '他人描述', '自我探討']
        num_labels = labels.__len__()
        return label_map, num_labels

    def _create_examples(self, lines, set_type):
        """Creates examples for the training and dev sets."""
        examples = []
        for (i, line) in enumerate(lines):
            guid = "%s-%s" % (set_type, i)
            label = tokenization.convert_to_unicode(line[0])
            text = tokenization.convert_to_unicode(line[1])
            examples.append(InputExample(guid=guid, text=text, label=label))
        return examples

def convert_single_example(ex_index, example, label_map,
                           max_seq_length, tokenizer):
    """Converts a single `InputExample` into a single `InputFeatures`."""

    if isinstance(example, PaddingInputExample):
        return InputFeatures(
            input_ids=[0] * max_seq_length,
            input_mask=[0] * max_seq_length,
            segment_ids=[0] * max_seq_length,
            label_ids=[0] * len(label_map),
            is_real_example=False)

    tokens_list = example.text
    tokens = []
    tags = []

    tokens.append("[CLS]")

    for i, word in enumerate(tokens_list):
        token = tokenizer.tokenize(word)
        tokens.extend(token)

    tokens.append("[SEP]")


    if len(tokens) >= max_seq_length:
        tokens = tokens[0:max_seq_length]

    

    segment_ids = [0] * max_seq_length
    input_ids = tokenizer.convert_tokens_to_ids(tokens)
    input_mask = [1] * len(input_ids)
    label_list = example.label.strip('[]').split(', ')
    multi_label_list = [0] * len(label_map)

    for label in label_list:
        label_index = label_map[label]
        multi_label_list[label_index] = 1

    # Zero-pad up to the sequence length.
    while len(input_ids) < max_seq_length:
        input_ids.append(0)
        input_mask.append(0)

    assert len(input_ids) == max_seq_length
    assert len(input_mask) == max_seq_length
    assert len(segment_ids) == max_seq_length

    if ex_index < 5:
        tf.logging.info("*** Example ***")
        tf.logging.info("guid: %s" % (example.guid))
        tf.logging.info("label: %s" % (example.label))
        tf.logging.info("tokens: %s" % " ".join(
            [tokenization.printable_text(x) for x in tokens]))
        tf.logging.info(
            "input_ids: %s" % " ".join([str(x) for x in input_ids]))
        tf.logging.info(
            "input_mask: %s" % " ".join([str(x) for x in input_mask]))
        tf.logging.info(
            "segment_ids: %s" % " ".join([str(x) for x in segment_ids]))
        #tf.logging.info("tags_ids: %s" % " ".join([str(x) for x in tags_ids]))
    feature = InputFeatures(
        label_ids=multi_label_list,
        input_ids=input_ids,
        input_mask=input_mask,
        segment_ids=segment_ids)
    return feature


def file_based_convert_examples_to_features(
        examples, label_map, max_seq_length, tokenizer, output_file):
    """Convert a set of `InputExample`s to a TFRecord file."""
    writer = tf.python_io.TFRecordWriter(output_file)
    for (ex_index, example) in enumerate(examples):
        if ex_index % 10000 == 0:
            tf.logging.info(
                "Writing example %d of %d" % (ex_index, len(examples)))
        feature = convert_single_example(ex_index, example, label_map,
                                        max_seq_length, tokenizer)

        def create_int_feature(values):
            f = tf.train.Feature(
                int64_list=tf.train.Int64List(value=list(values)))
            return f

        features = collections.OrderedDict()
        features["label_ids"] = create_int_feature(feature.label_ids)
        features["input_ids"] = create_int_feature(feature.input_ids)
        features["input_mask"] = create_int_feature(feature.input_mask)
        features["segment_ids"] = create_int_feature(feature.segment_ids)
        features["is_real_example"] = create_int_feature(
            [int(feature.is_real_example)])

        tf_example = tf.train.Example(
            features=tf.train.Features(feature=features))
        writer.write(tf_example.SerializeToString())
    writer.close()


def file_based_input_fn_builder(input_file, seq_length, is_training,
                                drop_remainder, num_labels):
    """Creates an `input_fn` closure to be passed to TPUEstimator."""

    name_to_features = {
        "label_ids": tf.FixedLenFeature([num_labels], tf.int64),
        "input_ids": tf.FixedLenFeature([seq_length], tf.int64),
        "input_mask": tf.FixedLenFeature([seq_length], tf.int64),
        "segment_ids": tf.FixedLenFeature([seq_length], tf.int64),
        "is_real_example": tf.FixedLenFeature([], tf.int64),
    }

    def _decode_record(record, name_to_features):
        """Decodes a record to a TensorFlow example."""
        example = tf.parse_single_example(record, name_to_features)

        # tf.Example only supports tf.int64, but the TPU only supports tf.int32.
        # So cast all int64 to int32.
        for name in list(example.keys()):
            t = example[name]
            if t.dtype == tf.int64:
                t = tf.to_int32(t)
            example[name] = t

        return example

    def input_fn(params):
        """The actual input function."""
        batch_size = params["batch_size"]

        # For training, we want a lot of parallel reading and shuffling.
        # For eval, we want no shuffling and parallel reading doesn't matter.
        d = tf.data.TFRecordDataset(input_file)
        if is_training:
            d = d.repeat()
            d = d.shuffle(buffer_size=100)

        d = d.apply(
            tf.contrib.data.map_and_batch(
                lambda record: _decode_record(record, name_to_features),
                batch_size=batch_size,
                drop_remainder=drop_remainder))

        return d

    return input_fn


def create_model(bert_config, input_ids, input_mask, segment_ids,intent_num_labels,use_one_hot_embeddings):
    """Creates a classification model."""
    model = modeling.BertModel(
        config=bert_config,
        is_training=False,
        input_ids=input_ids,
        input_mask=input_mask,
        token_type_ids=segment_ids,
        use_one_hot_embeddings=use_one_hot_embeddings)

    # In the demo, we are doing a simple classification task on the entire
    # segment.
    #
    # If you want to use the token-level output, use model.get_sequence_output()
    # instead.
    intent_output_layer = model.get_pooled_output()
    sequence_output_layer = model.get_sequence_output()

    hidden_size = sequence_output_layer.shape[-1].value

    ## intent loss
    intent_output_weights = tf.get_variable(
        "intent_output_weights", [intent_num_labels, hidden_size],
        # initializer=tf.truncated_normal_initializer(stddev=0.02)
        initializer=tf.contrib.layers.xavier_initializer()
    )

    intent_output_bias = tf.get_variable(
        "intent_output_bias", [intent_num_labels], initializer=tf.zeros_initializer())

    with tf.variable_scope("intent_loss"):

        intent_logits = tf.matmul(intent_output_layer, intent_output_weights, transpose_b=True)
        intent_logits = tf.nn.bias_add(intent_logits, intent_output_bias, name='intent_logits')
        intent_probabilities = tf.nn.sigmoid(intent_logits, name='intent_probabilities')



    return intent_probabilities


def f1_score(labels, predictions, weights=None, num_thresholds=200,
             metrics_collections=None, updates_collections=None, name=None):
    with variable_scope.variable_scope(
            name, 'f1', (labels, predictions, weights)):
        predictions, labels, weights = metrics_impl._remove_squeezable_dimensions(  # pylint: disable=protected-access
            predictions=predictions, labels=labels, weights=weights)
        # To account for floating point imprecisions / avoid division by zero.
        epsilon = 1e-7
        thresholds = [(i + 1) * 1.0 / (num_thresholds - 1)
                      for i in range(num_thresholds - 2)]
        thresholds = [0.0 - epsilon] + thresholds + [1.0 + epsilon]
        thresholds_tensor = tf.constant(thresholds)

        # Confusion matrix.
        values, update_ops = metrics_impl._confusion_matrix_at_thresholds(  # pylint: disable=protected-access
            labels, predictions, thresholds, weights, includes=('tp', 'fp', 'fn'))

        # Compute precision and recall at various thresholds.
        def compute_best_f1_score(tp, fp, fn, name):
            precision_at_t = math_ops.div(tp, epsilon + tp + fp,
                                          name='precision_' + name)
            recall_at_t = math_ops.div(tp, epsilon + tp + fn, name='recall_' + name)
            # Compute F1 score.
            f1_at_thresholds = (
                    2.0 * precision_at_t * recall_at_t /
                    (precision_at_t + recall_at_t + epsilon))

            best_f1 = math_ops.reduce_max(f1_at_thresholds)
            best_f1_index = tf.math.argmax(f1_at_thresholds)
            precision = precision_at_t[best_f1_index]
            recall = recall_at_t[best_f1_index]
            threshold = thresholds_tensor[best_f1_index]
            return best_f1, precision, recall, threshold

        def f1_across_replicas(_, values):
            best_f1, precision, recall, threshold = compute_best_f1_score(tp=values['tp'], fp=values['fp'],
                                                                          fn=values['fn'], name='value')
            if metrics_collections:
                ops.add_to_collections(metrics_collections, best_f1, precision, recall, threshold)
            return best_f1, precision, recall, threshold

        best_f1, precision, recall, threshold = distribution_strategy_context.get_replica_context().merge_call(
            f1_across_replicas, args=(values,))

        update_op = compute_best_f1_score(tp=update_ops['tp'], fp=update_ops['fp'],
                                          fn=update_ops['fn'], name='update')
        if updates_collections:
            ops.add_to_collections(updates_collections, update_op)

        # return (best_f1, precision, recall, threshold), update_op
        return (best_f1, update_op), (precision, update_op), (recall, update_op), (threshold, update_op)
        # return best_f1, precision, recall, threshold


def model_fn_builder(bert_config, num_labels, init_checkpoint,use_tpu,
                     use_one_hot_embeddings):
    """Returns `model_fn` closure for TPUEstimator."""

    def model_fn(features, labels, mode, params):  # pylint: disable=unused-argument
        """The `model_fn` for TPUEstimator."""

        tf.logging.info("*** Features ***")
        for name in sorted(features.keys()):
            tf.logging.info("  name = %s, shape = %s" % (name, features[name].shape))

        input_ids = features["input_ids"]
        input_mask = features["input_mask"]
        segment_ids = features["segment_ids"]
        label_ids = features["label_ids"]
        probabilities = create_model(bert_config, input_ids, input_mask, segment_ids,
                                     num_labels, use_one_hot_embeddings)

        tvars = tf.trainable_variables()
        scaffold_fn = None
        if init_checkpoint:
            (assignment_map, initialized_variable_names
             ) = modeling.get_assignment_map_from_checkpoint(tvars, init_checkpoint)
            if use_tpu:

                def tpu_scaffold():
                    tf.train.init_from_checkpoint(init_checkpoint, assignment_map)
                    return tf.train.Scaffold()

                scaffold_fn = tpu_scaffold
            else:
                tf.train.init_from_checkpoint(init_checkpoint, assignment_map)



        output_spec = tf.contrib.tpu.TPUEstimatorSpec(
            mode=mode,
            predictions={
                         "intent_predicted": probabilities,
                         "label_ids": label_ids,
                         "input_ids": input_ids,
                         "mask_length": tf.reduce_sum(input_mask, axis=1)},
            scaffold_fn=scaffold_fn)
        return output_spec

    return model_fn


def predict(output_dir,label_map,tokenizer,num_labels,estimator,predict_examples):
    if use_tpu:
        # TPU requires a fixed batch size for all batches, therefore the number
        # of examples must be a multiple of the batch size, or else examples
        # will get dropped. So we pad with fake examples which are ignored
        # later on.
        while len(predict_examples) % predict_batch_size != 0:
            predict_examples.append(PaddingInputExample())

    predict_file = os.path.join(output_dir, "predict.tf_record")
    file_based_convert_examples_to_features(predict_examples, label_map,
                                            max_seq_length, tokenizer, predict_file)


    predict_drop_remainder = True if use_tpu else False
    predict_input_fn = file_based_input_fn_builder(
        input_file=predict_file,
        seq_length=max_seq_length,
        is_training=False,
        drop_remainder=predict_drop_remainder,
        num_labels=num_labels)

    results = [row['intent_predicted'] for row in list(estimator.predict(input_fn=predict_input_fn))]
    float_results = []
    label_map = list(label_map.keys())
    for row in results.copy():
        float_results.append({label_map[idx]:float(r) for idx, r in enumerate(row)})
    return float_results


class ModelInterface:
    def __init__(self,bert_config_file,output_dir,vocab_file,init_checkpoint):
        self.vocab_file = vocab_file
        self.output_dir = output_dir
        self.init_checkpoint = init_checkpoint
        tf.logging.set_verbosity(tf.logging.ERROR)

        processors = {
            "customized": customizedProcessor
        }

        tokenization.validate_case_matches_checkpoint(do_lower_case,
                                                      self.init_checkpoint)

        if not  do_train and not  do_eval and not  do_predict and not  do_export:
            raise ValueError(
                "At least one of `do_train`, `do_eval`, `do_predict' or 'do_export' must be True.")

        bert_config = modeling.BertConfig.from_json_file(bert_config_file)

        if  max_seq_length > bert_config.max_position_embeddings:
            raise ValueError(
                "Cannot use sequence length %d because the BERT model "
                "was only trained up to sequence length %d" %
                ( max_seq_length, bert_config.max_position_embeddings))

        tf.gfile.MakeDirs( output_dir)


        if task_name not in processors:
            raise ValueError("Task not found: %s" % (task_name))

        self.processor = processors[task_name]()

        self.label_map, self.num_labels = self.processor.get_labels_info(self.output_dir)

        self.tokenizer = tokenization.FullTokenizer(
            vocab_file= self.vocab_file, do_lower_case= do_lower_case)

        tpu_cluster_resolver = None
        if use_tpu and tpu_name:
            tpu_cluster_resolver = tf.contrib.cluster_resolver.TPUClusterResolver(
                tpu_name, zone=tpu_zone, project=gcp_project)

        is_per_host = tf.contrib.tpu.InputPipelineConfig.PER_HOST_V2
        run_config = tf.contrib.tpu.RunConfig(
            cluster=tpu_cluster_resolver,
            master=master,
            model_dir=output_dir,
            save_checkpoints_steps=save_checkpoints_steps,
            tpu_config=tf.contrib.tpu.TPUConfig(
                iterations_per_loop=iterations_per_loop,
                num_shards=num_tpu_cores,
                per_host_input_for_training=is_per_host))

        train_examples = None
        num_train_steps = None
        num_warmup_steps = None

        model_fn = model_fn_builder(
            bert_config=bert_config,
            num_labels=self.num_labels,
            init_checkpoint=self.init_checkpoint,
            use_tpu=use_tpu,
            use_one_hot_embeddings=use_one_hot_embeddings)



        # If TPU is not available, this will fall back to normal Estimator on CPU
        # or GPU.
        self.estimator = tf.contrib.tpu.TPUEstimator(
            use_tpu=use_tpu,
            model_fn=model_fn,
            config=run_config,
            train_batch_size=train_batch_size,
            eval_batch_size=eval_batch_size,
            predict_batch_size=predict_batch_size)

    def predict(self, price_str):

        predict_examples = self.processor.get_strs_examples(price_str,self.output_dir)
        return predict(self.output_dir,self.label_map, self.tokenizer, self.num_labels, self.estimator, predict_examples)



