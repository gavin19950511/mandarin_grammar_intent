# mandarin_grammar_client
refactor from [microServiceStructure](https://github.com/Chunshan-Theta/microServiceStructure)

### folder
- api_server: tornado server
- LocalStorage-api
- LocalStorage-mdl:
    - bert_wwm: pretrain model
    - output: model files
- mdl_server: model worker (with redis)
- redis_server: redis server

### quick to start
```
docker-compose build
docker-compose up
```