import uuid

from .modelHandler import new_work, get_responds, modelHandler
from ...util.aredis_queue import Queue
from ..apiHandlerBase import APIHandlerBase
import json
import asyncio
import numpy
from numpy import argmax

redis_to_MDL = Queue(name="MDL", namespace="common")
redis_to_API = Queue(name="API", namespace="common")
"""
curl --location --request POST '127.0.0.1:8081/model/service' \
--header 'Content-Type: application/json' \
--data-raw '{
    "texts":[
        "這個故事好無聊"
    ],
    "custom_model_score":[1,1,1,1,1],
    "classification_probabilities":[
        {
            "事物說明": 0.4,
            "他人描述": 0.2,
            "自我探討": 0.4
        },
        {
            "事物說明": 0.2,
            "他人描述": 0.2,
            "自我探討": 0.6
        },
        {
            "事物說明": 0.1,
            "他人描述": 0.1,
            "自我探討": 0.8
        },
        {
            "事物說明": 0.05,
            "他人描述": 0.9,
            "自我探討": 0.05
        },
        {
            "事物說明": 0.2,
            "他人描述": 0.2,
            "自我探討": 0.8
        }
        
    ]
}'
"""

class serviceHandler(modelHandler):

    async def post(self):
            body:dict = self.request_body_json.copy()



            ##
            text = body.get("texts",[])
            assert len(text) == 1, "`texts` only support count 1"
            query_text = text[0]
            eid = await new_work(query_text)
            re = await get_responds(eid=eid)
            while re is None:
                await asyncio.sleep(1)
                re = await get_responds(eid=eid)
            result = re[query_text]
            intent_items_name = list(result.keys())

            ##
            ##
            #classification_score = [0.99, 0.005, 0.005]
            classification_score = [result[name] for name in intent_items_name]
            # classification_probabilities = [
            #     [1, 0, 0],
            #     [0, 1, 0],
            #     [0, 0, 1],
            # ]
            classification_probabilities_rows = body.get("classification_probabilities", [])
            classification_probabilities = []
            for row in classification_probabilities_rows:
                row_probabilities = [row[name] for name in intent_items_name]
                classification_probabilities.append(row_probabilities)

            # custom_model_score = [1, 0, 0]
            custom_model_score = body.get("custom_model_score", [])

            re = list(FlexPostModel(classification_score=classification_score,
                               classification_probabilities=classification_probabilities,
                               custom_model_score=custom_model_score).predict())

            """
            # build the responds
            """
            resp = {
                "texts": query_text,
                "result": result,
                "re": re
            }

            """
            # respond the request
            """
            self.write_json(resp)


##
class FlexPostModel:
    def __init__(self,classification_probabilities,classification_score=None,custom_model_score=None,alpha=0.5):
        self.classification_probabilities = classification_probabilities
        self.classification_score = classification_score if classification_score is not None else [1]*len(classification_probabilities[0])
        self.custom_model_score = custom_model_score if custom_model_score is not None else [1]*len(classification_probabilities)
        self.custom_class_count = len(self.custom_model_score)
        self.alpha = alpha

        #
        for row in self.classification_probabilities:
            assert sum(row) > 0
            assert len(row) == len(self.classification_score)
        assert len(self.classification_probabilities) == self.custom_class_count
        assert 0<self.alpha<1

    @staticmethod
    def average_prob(agr_list):
        return numpy.array([i/sum(agr_list) for i in agr_list])

    def predict(self,classification_score=None,custom_model_score=None):

        #
        classification_score = self.average_prob(self.classification_score) if classification_score is None else self.average_prob(classification_score)
        custom_model_score = self.average_prob(self.custom_model_score) if custom_model_score is None else self.average_prob(custom_model_score)

        #
        predict_classification = []
        for gar_prob in self.classification_probabilities:
            gar_average_prob = self.average_prob(gar_prob)
            predict_classification.append(sum(gar_average_prob * classification_score))

        predict_classification = numpy.array(predict_classification) * self.alpha
        predict_classification += ([1-self.alpha] * custom_model_score)
        return self.average_prob(predict_classification)




# ## 規則模型與預先訊聯模型分類數一致
# classification_score = [0.99,0.005,0.005]
# classification_probabilities = [
#     [1,0,0],
#     [0,1,0],
#     [0,0,1],
# ]
# custom_model_score = [1,0,0]
# md = FlexPostModel(classification_score=classification_score,classification_probabilities=classification_probabilities,custom_model_score=custom_model_score)
# assert 0 == argmax(md.predict(), axis=0)
#
#
# ## 規則模型與預先訊聯模型分類數不一致
# classification_score = [0.33,0.33,0.33]
# classification_probabilities = [
#     [0.33,0.33,0.33],
#     [0.33,0.33,0.33],
#     [0.33,0.33,0.33],
#     [0.33,0.33,0.33],
#     [0.33,0.33,0.33]
# ]
# custom_model_score = [0.2,0.2,0.2,0.2,0.2]
# md = FlexPostModel(classification_score=classification_score,classification_probabilities=classification_probabilities,custom_model_score=custom_model_score)
# assert 0 == argmax(md.predict(), axis=0)
#
#
#
#
#
# ## 預先訓練模型無法識別的狀況，採用規則辨識
# classification_score = [0.33,0.33,0.33]
# classification_probabilities = [
#     [1,0,0],
#     [0,1,0],
#     [0,0,1],
#     [1,0,0],
# ]
# custom_model_score = [0.7,0.1,0.1,0.1]
# md = FlexPostModel(classification_score=classification_score,classification_probabilities=classification_probabilities,custom_model_score=custom_model_score)
# assert 0 == argmax(md.predict(), axis=0)
#
#
# ## 規則辨識無法識別的狀況，採用預先訓練模型
# classification_score = [0.99,0.005,0.005]
# classification_probabilities = [
#     [1,0,0],
#     [0,1,0],
#     [0,0,1],
#     [0.5,0,0.5],
# ]
# custom_model_score = [0.25,0.25,0.25,0.25]
# md = FlexPostModel(classification_score=classification_score,classification_probabilities=classification_probabilities,custom_model_score=custom_model_score)
# assert 0 == argmax(md.predict(), axis=0)
#
#
# ## 無規則辨識，採用預先訓練模型
# classification_score = [0.99,0.005,0.005]
# classification_probabilities = [
#     [1,0,0],
#     [0,1,0],
#     [0,0,1],
#     [0.5,0,0.5],
# ]
# md = FlexPostModel(classification_score=classification_score,classification_probabilities=classification_probabilities)
# assert 0 == argmax(md.predict(), axis=0)
#
#
# ## 規則辨識與預先訓練模型預測採用權重
# classification_score = [0.99,0.005,0.005]
# classification_probabilities = [
#     [1,0,0],
#     [0,1,0],
#     [0,0,1],
#     [0,0.5,0.5],
# ]
# custom_model_score = [0.05,0.05,0.05,0.75]
# md = FlexPostModel(classification_score=classification_score,classification_probabilities=classification_probabilities,custom_model_score=custom_model_score,alpha=0.9)
# assert 0 == argmax(md.predict(), axis=0)
# md = FlexPostModel(classification_score=classification_score,classification_probabilities=classification_probabilities,custom_model_score=custom_model_score,alpha=0.1)
# assert 3 == argmax(md.predict(), axis=0)