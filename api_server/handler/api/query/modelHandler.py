import uuid
from ...util.aredis_queue import Queue
from ..apiHandlerBase import APIHandlerBase
import json
import asyncio
redis_to_MDL = Queue(name="MDL", namespace="common")
redis_to_API = Queue(name="API", namespace="common")


class modelHandler(APIHandlerBase):
    @staticmethod
    def remove_sensitive_word(str):
        return str.replace(",", " ")

    @staticmethod
    async def request_predict(texts):
        eid = await new_work(texts)
        re = await get_responds(eid=eid)
        while re is None:
            await asyncio.sleep(1)
            re = await get_responds(eid=eid)
        return re

    @staticmethod
    def combine_result(re: dict):
        re_dict = {}
        for row in re.values():
            for key, val in row.items():
                if key not in re_dict:
                    re_dict[key] = 0
                re_dict[key] += val
        return re_dict


class modelGroupHandler(modelHandler):
    async def post(self):
        """
        curl --location --request POST '127.0.0.1:8081/model/default' \
        --header 'Content-Type: application/json' \
        --data-raw '{
            "text_groups":[
                ["這個故事好無聊","你真得很糟糕","我感覺到很憂鬱"],
                ["這個故事好無聊","你真得很糟糕","我感覺到很憂鬱"],
                ["這個故事好無聊","你真得很糟糕","我感覺到很憂鬱"]
            ]
        }'
        """
        body:dict = self.request_body_json.copy()
        text_groups = body.get("text_groups", [])
        text_group_responds = []
        for texts in text_groups:
            texts = [self.remove_sensitive_word(row) for row in texts]
            texts = ",".join(texts)
            re = await self.request_predict(texts=texts)

            #
            re_dict = self.combine_result(re)

            text_group_responds.append(re_dict)


        """
        # build the responds
        """
        resp = {
            "texts": text_groups,
            "re": text_group_responds
        }

        """
        # respond the request
        """
        self.write_json(resp)


async def new_work(item):
    print(f"new_work: {item}")
    uuid_task = str(uuid.uuid4())
    await redis_to_MDL.enqueue_with_json(eid=uuid_task, data=item)
    return uuid_task


async def get_responds(eid):
    res = await redis_to_API.get_msg_by_direct_id(id=eid)
    print(f"get_responds: {res}")
    return res

