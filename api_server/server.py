import os

import tornado.httpserver
import tornado.netutil
import tornado.options
from tornado import ioloop
from tornado.options import define, options, parse_command_line
from tornado.log import access_log

from handler.api.query.modelHandler import modelGroupHandler
from handler.api.query.testHandler import testHandler
from handler.api.query.serviceHandler import serviceHandler

define("subpath", group='Webserver', type=str, default="", help="Url subpath (such as /nebula)")
define("port", group='Webserver', type=int, default='8081', help="Run on the given port")
define("server_ip", group='Webserver', type=int, default='0.0.0.0', help="IP")
def main():

    #
    tornado.options.parse_command_line()
    parse_command_line()
    options.subpath = options.subpath.strip('/')
    if options.subpath:
        options.subpath = '/' + options.subpath
    io_loop = ioloop.IOLoop.instance()

    # Start application

    # urls initial
    urls = [
        [r'/status/Test', testHandler],
        [r'/model/default', modelGroupHandler],
        [r'/model/service', serviceHandler]
    ]

    # Add subpath to urls
    for u in urls:
        u[0] = options.subpath + u[0]
    # tornado_opentracing.init_tracing()
    app = tornado.web.Application(
        handlers=urls,
        template_path=os.path.join(os.path.dirname(__file__), "template"),
        static_path=os.path.join(os.path.dirname(__file__), 'static'),
        debug=True,
        allow_remote_access=True
    )

    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port, '0.0.0.0')
    access_log.info('Server is running at http://%s:%s%s' % (options.server_ip, options.port, options.subpath))
    access_log.debug('Quit the server with Control-C')

    io_loop.start()


if __name__ == "__main__":
    main()
